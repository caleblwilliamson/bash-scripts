#!/usr/bin/env bash

# Directory argument
filedir=$1
cutcount=$(echo $1 | wc -c)
echo $cutcount

# Define sizes to match against
large="^[0-9].*[G|M]"
medium="^[5-9][0-9]*K"
small="^[0-4][0-9]*K"

# Out put all file names to files.txt
ls -lh $filedir/* | awk '{print $9}' > $filedir/files.txt
sleep 1

# Create sorting directories
mkdir $filedir/small $filedir/medium $filedir/large

# Compare the file size taken from ls -lh to the regex values defined above
while read file; do

    filesize=$(ls -lh $file | awk '{print $5}')
    echo "${file} is ${filesize}"
    
    filechars=$(echo $file | wc -c)
    filename=$(echo $file | cut -c $cutcount-$filechars)

    if [[ ${filesize} =~ ${small} ]]
    then
        mv $file $filedir/small$filename

    elif [[ ${filesize} =~ ${medium} ]]
    then
        mv $file $filedir/medium$filename

    elif [[ ${filesize} =~ ${large} ]]
    then
        mv $file $filedir/large$filename

    else # Accounting for anything smaller than 1KB 
        mv $file $filedir/small$filename
    fi

done < $filedir/files.txt
rm -f $filedir/files.txt