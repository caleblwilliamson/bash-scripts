#!/usr/bin/env bash

# Output all users and their home dir
cat /etc/passwd | cut -d: -f1,6

userlist=$(cat /etc/passwd | cut -d: -f1,6)
currentUsers=$(echo $userlist | md5sum | cut -c 1-32)
dt=$(date '+%d/%m/%Y %H:%M:%S')

if [ ! -f /var/log/current_users ]
then
    cat /etc/passwd | cut -d: -f1,6 > /var/log/current_users
    usersFile=$(tail -n 1 /var/log/current_users)
else
    usersFile=$(tail -n 1 /var/log/current_users)
fi

if [ "$currentUsers" = "$usersFile" ]
then
    echo "current_users match"
else
    echo "users hash changed"
    echo "hash changed from: $usersFile to $currentUsers @ $dt" >> /var/log/user_changes
    echo "$currentUsers" > /var/log/current_users;
fi